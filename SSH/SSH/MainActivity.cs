﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Rebex.Net;
using Rebex.TerminalEmulation;
using Renci.SshNet;

namespace SSH
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]

    public class MainActivity : AppCompatActivity
    {
        EditText hostName, user, password, port;
        Button btnConnect, btnDisconnect, btnBack;
        SshClient client;
        Ssh ssh;
        ListView list;
        String path;
        Scripting scripting;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            UserDialogs.Init(this);
            Rebex.Licensing.Key = "==AdN+3SSfZJukOUMQpDGw3TnV7LkFkxBGUiQa3Dez+uq0==";

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            Instancias();
            Acciones();
        }

        private void Instancias()
        {
            hostName = FindViewById<EditText>(Resource.Id.hostname);
            user = FindViewById<EditText>(Resource.Id.user);
            port = FindViewById<EditText>(Resource.Id.port);
            password = FindViewById<EditText>(Resource.Id.password);
            btnConnect = FindViewById<Button>(Resource.Id.btnConnect);
            btnDisconnect = FindViewById<Button>(Resource.Id.btnDisconnect);
            btnBack = FindViewById<Button>(Resource.Id.btnBack);
            list = FindViewById<ListView>(Resource.Id.listView);
        }

        private void Acciones()
        {
            btnConnect.Click += OnConnectSSH;
            btnDisconnect.Click += OnDisconnectSSH;
            btnBack.Click += OnBackClick;
            list.ItemClick += OnListClicked;
        }

        private void OnBackClick(object sender, EventArgs e)
        {
            BackDirectoryRebex();
        }

        private void OnListClicked(object sender, AdapterView.ItemClickEventArgs e)
        {
            //Toast.MakeText(this, list.GetItemAtPosition(e.Position).ToString(), ToastLength.Long).Show();
            String selected = list.GetItemAtPosition(e.Position).ToString();

            if (selected.Contains("."))
            {
                Toast.MakeText(this, "No se puede abrir el archivo", ToastLength.Long).Show();
                return;
            }

            string directory = FormatedPath(selected);
            ChangeDirectoryRebex(directory);

        }

        private void OnDisconnectSSH(object sender, EventArgs e)
        {
            try
            {
                //client.Disconnect();
                ssh.Disconnect();

                if (!client.IsConnected)
                {
                    list.Adapter = null;
                    Toast.MakeText(this, "Cliente desconectado", ToastLength.Long).Show();
                    btnDisconnect.Enabled = false;
                    btnBack.Enabled = false;
                    btnConnect.Enabled = true;
                    path = String.Empty;
                }

            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
            }
        }

        private void OnConnectSSH(object sender, EventArgs e)
        {
            // Obtener los datos introducidos
            var _hostName = hostName.Text;
            var _user = user.Text;
            var _password = password.Text;
            var _port = port.Text;

            client = new SshClient(_hostName, Int32.Parse(_port), _user, _password);
            ssh = new Rebex.Net.Ssh();

            try
            {
                //ClientConnect(client);
                ClientConnectRebex(ssh);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                btnConnect.Enabled = true;
            }
        }

        private void ClientConnect(SshClient client)
        {

            client.Connect();

            if (client.IsConnected)
            {
                Toast.MakeText(this, "Cliente conectado", ToastLength.Long).Show();
                btnDisconnect.Enabled = true;
            }

            SshCommand sc2 = client.CreateCommand("ls");
            sc2.Execute();
            string answer = sc2.Result;
            string[] words = answer.Split('\n');
            words = words.Where(w => w != words[words.Length - 1]).ToArray();

            ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, words);
            list.Adapter = adaptador;
        }

        private void ClientConnectRebex(Rebex.Net.Ssh ssh)
        {

            ssh.Connect(hostName.Text, Int32.Parse(port.Text));
            ssh.Login(user.Text, password.Text);

            if (ssh.IsConnected)
            {
                Toast.MakeText(this, "Cliente conectado", ToastLength.Long).Show();
                btnDisconnect.Enabled = true;
                btnBack.Enabled = true;
                btnConnect.Enabled = false;
            }

            scripting = ssh.StartScripting();
            scripting.DetectPrompt();
            scripting.SendCommand("ls");

            string line = scripting.ReadUntilPrompt();
            string[] words = line.Split(' ');

            List<string> temp = new List<string>();
            foreach (var item in words)
            {
                if (!String.IsNullOrEmpty(item))
                    temp.Add(item);
            }

            RellenarLista(temp, list);

        }

        private void ChangeDirectoryRebex(string directory)
        {
            scripting.SendCommand("pwd");
            string pwd = FormatedPath(scripting.ReadUntilPrompt()) + "/";

            scripting.SendCommand("cd " + pwd + directory);
            //scripting.SendCommand("ls ");
            string line = ssh.RunCommand("cd " + pwd + directory + "; ls");
            //string line = scripting.ReadUntilPrompt();
            string[] words = line.Split('\n');
            //string[] words = line.Split(' ');

            List<string> temp = new List<string>();
            foreach (var item in words)
            {
                if (!String.IsNullOrEmpty(item))
                    temp.Add(item);
            }

            RellenarLista(temp, list);
        }

        private void BackDirectoryRebex()
        {
            scripting.SendCommand("cd ..");
            scripting.SendCommand("pwd");
            string pwd = FormatedPath(scripting.ReadUntilPrompt()) + "/";

            scripting.SendCommand("cd " + pwd);
            //scripting.SendCommand("ls ");
            string line = ssh.RunCommand("cd " + pwd + "; ls");
            string[] words = line.Split('\n');

            List<string> temp = new List<string>();
            foreach (var item in words)
            {
                if (!String.IsNullOrEmpty(item))
                    temp.Add(item);
            }

            RellenarLista(temp, list);
        }

        private void RellenarLista(List<string> temp, ListView list)
        {
            list.Adapter = null;
            ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, temp);
            list.Adapter = adaptador;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private string FormatedPath(string path)
        {
            string replace = string.Empty;

            if (path != null && path.Contains(" "))
                replace = path.Replace(" ", "\\ ");
            else
                replace = path;

            return replace.Trim();
        }
    }
}

